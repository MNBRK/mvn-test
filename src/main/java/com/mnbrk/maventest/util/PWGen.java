package com.mnbrk.maventest.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.Hash;
import org.apache.shiro.crypto.hash.HashRequest;
import org.apache.shiro.crypto.hash.HashService;
import org.apache.shiro.crypto.hash.format.Shiro1CryptFormat;
import org.apache.shiro.util.ByteSource;

public class PWGen {
    public List<String> generateNewHashedPassword() {
        DefaultHashService defaultHashService = new DefaultHashService();
        defaultHashService.setHashAlgorithmName("SHA-512");
        defaultHashService.setHashIterations(2048);
        defaultHashService.setGeneratePublicSalt(true);
        
        Shiro1CryptFormat shiro1CryptFormat = new Shiro1CryptFormat();
        String pw = generateCommonLangPassword();
        Hash hash = hashPassword(defaultHashService, pw);

        List<String> pwList = new ArrayList<>();
        pwList.add(pw);
        pwList.add(shiro1CryptFormat.format(hash));

        return pwList;
    }

    private Hash hashPassword(HashService hashService, Object text) {
        ByteSource textBytes = ByteSource.Util.bytes(text);
        if(textBytes == null || textBytes.isEmpty()) return null;
        HashRequest hashRequest = createHashRequest(textBytes);
        return hashService.computeHash(hashRequest);
    }

    private String generateCommonLangPassword() {
        String upperCaseLetters = RandomStringUtils.random(12, 65, 90, true, true);
        String lowerCaseLetters = RandomStringUtils.random(12, 97, 122, true, true);
        String numbers = RandomStringUtils.randomNumeric(4);
        String specialChar = RandomStringUtils.random(2, 33, 47, false, false);
        String totalChars = RandomStringUtils.randomAlphanumeric(2);
        String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
          .concat(numbers)
          .concat(specialChar)
          .concat(totalChars);
        List<Character> pwdChars = combinedChars.chars()
          .mapToObj(c -> (char) c)
          .collect(Collectors.toList());
        Collections.shuffle(pwdChars);
        String password = pwdChars.stream()
          .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
          .toString();
        return password;
    }

    private HashRequest createHashRequest(ByteSource byteSource) {
        return new HashRequest.Builder().setSource(byteSource).build();
    }
}
