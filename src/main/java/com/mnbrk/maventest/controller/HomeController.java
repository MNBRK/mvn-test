package com.mnbrk.maventest.controller;

import com.mnbrk.maventest.util.PWGen;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @Value("${spring.application.name}")
    String appName;

    @Value("${msg.GenPass}")
    String genPassMsg;

    @GetMapping("/")
    public String homePage(Model model) {
        model.addAttribute("appName", appName);
        model.addAttribute("genPassMsg", genPassMsg);
        return "home";
    }

    @GetMapping("/genPass")
    public String passPage(Model model) {
        PWGen pwGen = new PWGen();
        model.addAttribute("appName", appName);
        model.addAttribute("pass", pwGen.generateNewHashedPassword().get(0));
        model.addAttribute("hash", pwGen.generateNewHashedPassword().get(1));
        return "pass";
    }
}